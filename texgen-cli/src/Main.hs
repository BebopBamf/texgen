{-# LANGUAGE NoImplicitPrelude #-}

module Main (main) where

import Options.Applicative
import RIO
import RIO.Process
import Texgen (run, App(..))

data Options = Options
    { optionsVerbose :: !Bool
    , optionsVersion :: !Bool
    , optionsPath :: !FilePath
    }
    deriving (Show)

options :: Parser Options
options =
    Options
        <$> switch
            ( long "verbose"
                <> short 'v'
                <> help "Enable verbose mode"
            )
        <*> switch
            ( long "version"
                <> short 'V'
                <> help "Show version information"
            )
        <*> argument
            str
            ( metavar "PATH"
                <> help "Document directory"
            )

getOptions :: IO Options
getOptions = execParser opts
  where
    opts =
        info
            (options <**> helper)
            ( fullDesc
                <> progDesc "A LaTeX project generator, written in Haskell"
                <> header "texgen - a latex project generator"
            )

-- | The main entry point.
main :: IO ()
main = do
    opts <- getOptions

    lo <- logOptionsHandle stdout (optionsVerbose opts)
    pc <- mkDefaultProcessContext

    withLogFunc lo $ \lf ->
        let app =
                App
                    { appLogFunc = lf
                    , appProcessContext = pc
                    }
         in runRIO app run
