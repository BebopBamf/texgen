{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec
import Test.Hspec.Megaparsec
import Text.Megaparsec
import Texgen.Config
import RIO

main :: IO ()
main = hspec $ do
    describe "stringLiteral" $ do
        it "parses string literal" $ do
            let input = "\'foo\'"
            parse stringLiteral "" input `shouldParse` "foo"
