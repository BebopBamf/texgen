{-# LANGUAGE OverloadedStrings #-}

{- |
	Module      :  Texgen
	Description :  A Library for generating LaTeX projects from templates
	Copyright   :  2024 Euan Mendoza
	License     :  GNU GPL-3.0
	Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
    Stability   :  experimental
	Portability :  portable
-}
module Texgen (
    run,
    App (..),
) where

import RIO
import RIO.Process

data TexConfig = TexConfig
    { texConfigPath :: !FilePath
    , texConfigTitle :: !ByteString
    , texConfigAuthor :: !ByteString
    , texConfigDate :: !ByteString
    }

data App = App
    { appLogFunc :: !LogFunc
    , appProcessContext :: !ProcessContext
    }

type AppM = RIO App

instance HasLogFunc App where
    logFuncL = lens appLogFunc (\x y -> x{appLogFunc = y})

instance HasProcessContext App where
    processContextL = lens appProcessContext (\x y -> x{appProcessContext = y})

run :: AppM ()
run = do
    -- path <- asks $ optionsPath . appOptions
    -- logInfo $ "Path: " <> displayShow path
    logInfo "We're inside the application!"
