{- |
	Module      :  Texgen.Config
	Description :  Configuration file for the texgen library
	Copyright   :  2024 Euan Mendoza
	License     :  GNU GPL-3.0
	Maintainer  :  Euan Mendoza <bebopbamf@effectfree.dev>
    Stability   :  experimental
	Portability :  portable
-}
module Texgen.Config (spaceConsumer, lexeme, symbol, stringLiteral) where

import RIO
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import qualified RIO.Text as T

data Template = Template
    { templateName :: Text
    , templatePath :: FilePath
    } deriving (Show)

type Parser = Parsec Void Text

spaceConsumer :: Parser ()
spaceConsumer = L.space space1 empty empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceConsumer

symbol :: Text -> Parser Text
symbol = L.symbol spaceConsumer

stringLiteral :: Parser Text
stringLiteral = char '\'' >> manyTill L.charLiteral (char '\'') <&> T.pack
